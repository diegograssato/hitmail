<?php

namespace HitMail\Service\Factory;

use Zend\ServiceManager\ServiceLocatorInterface,
    Zend\ServiceManager\FactoryInterface,
    HitMail\Service\Mail as ServiceMail,
    Zend\Mail\Transport\SmtpOptions,
    Zend\Mail\Transport\Smtp,
    HitMail\Options\MailOptions,
    HitMail\Service\Template as ServiceTemplate;

class Mail implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {

        //var_dump($serviceLocator);die;
        $config = $serviceLocator->get('config');

        if (isset($config['HitMail']['transport']['smtpOptions'])) {

            $smtpOptions = new SmtpOptions($config['HitMail']['transport']['smtpOptions']);
            $transport = new Smtp($smtpOptions);

            $options = new MailOptions($config['HitMail']['options']);
        } else {
            throw new \Exception('Você precisa configurar o STMP Options no module.config.php');
        }

       return new ServiceMail($transport, $options, new ServiceTemplate($serviceLocator) );
    }
}