<?php

namespace HitMail\Service\Factory;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\FactoryInterface;
use HitMail\Service\Mandrill as ServiceMail;
use HitMail\Service\Template as ServiceTemplate;

class Mandrill implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config = $serviceLocator->get('config');

        if (isset($config['HitMail']['mandrill']))
            $consumer_key = $config['HitMail']['mandrill']['key'];

       return new ServiceMail($consumer_key, new ServiceTemplate($serviceLocator));
    }
}