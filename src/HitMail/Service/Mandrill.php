<?php

namespace HitMail\Service;

use Zend\Mime\Part as MimePart;
use Zend\Mail\Message;
use HitMail\Options\MandrillMessage as MimeMessage;
use HitMail\Service\MailInterface;
use HitMail\Service\Template;

class Mandrill implements MailInterface
{
    protected $consumer_key;
    protected $options;
    protected $template;

    protected $subject;
    protected $to;
    protected $from;
    protected $fromName;
    protected $data;
    protected $viewTemplate;

    public function __construct($consumer_key, Template $template)
    {
        $this->consumer_key = $consumer_key;
        $this->template = $template;
    }

    public function execute()
    {
        $message = new MimeMessage();
        $html = $this->getTemplate()->render($this->getViewTemplate(), $this->getData());
        $message
            ->setFromEmail($this->getFrom())
            ->setFromName($this->getFromName())
            ->addTo($this->getTo())
            ->setSubject($this->getSubject())
            ->addHeader('Reply-To',$this->getFrom())
            ->setHtml($html);

        if (count($message->getBccAddress()) > 0) {
             $message->setBccAddress($message->getBccAddress());
         }
        return $message->toArray();
    }

    public function send()
    {

        try {
            $mandrill = new \Mandrill($this->consumer_key);
            $mandrill->messages->send($this->execute());

        } catch (\Exception $e) {
            echo $e->getMessage();
        }

    }

    /**
     * Gets the value of transport.
     *
     * @return mixed
     */
    public function getTransport()
    {
        return $this->transport;
    }

    /**
     * Sets the value of transport.
     *
     * @param mixed $transport the transport
     *
     * @return self
     */
    public function setTransport($transport)
    {
        $this->transport = $transport;

        return $this;
    }

    /**
     * Gets the value of options.
     *
     * @return mixed
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Sets the value of options.
     *
     * @param mixed $options the options
     *
     * @return self
     */
    public function setOptions($options)
    {
        $this->options = $options;

        return $this;
    }

    /**
     * Gets the value of template.
     *
     * @return mixed
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * Sets the value of template.
     *
     * @param mixed $template the template
     *
     * @return self
     */
    public function setTemplate($template)
    {
        $this->template = $template;

        return $this;
    }

    /**
     * Gets the value of subject.
     *
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Sets the value of subject.
     *
     * @param mixed $subject the subject
     *
     * @return self
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Gets the value of to.
     *
     * @return mixed
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * Sets the value of to.
     *
     * @param mixed $to the to
     *
     * @return self
     */
    public function setTo($to)
    {
        $this->to = $to;

        return $this;
    }

    /**
     * Gets the value of from.
     *
     * @return mixed
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * Sets the value of from.
     *
     * @param mixed $from the from
     *
     * @return self
     */
    public function setFrom($from)
    {
        $this->from = $from;

        return $this;
    }

    /**
     * Gets the value of fromName.
     *
     * @return mixed
     */
    public function getFromName()
    {
        return $this->fromName;
    }

    /**
     * Sets the value of fromName.
     *
     * @param mixed $from the fromName
     *
     * @return self
     */
    public function setFromName($fromName)
    {
        $this->fromName = $fromName;

        return $this;
    }

    /**
     * Gets the value of data.
     *
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Sets the value of data.
     *
     * @param mixed $data the data
     *
     * @return self
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Gets the value of viewTemplate.
     *
     * @return mixed
     */
    public function getViewTemplate()
    {
        return $this->viewTemplate;
    }

    /**
     * Sets the value of viewTemplate.
     *
     * @param mixed $viewTemplate the view template
     *
     * @return self
     */
    public function setViewTemplate($viewTemplate)
    {
        $this->viewTemplate = $viewTemplate;

        return $this;
    }
}