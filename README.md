HitMail
================

Installation
---------------
Download this module into your vendor folder.

After done the above steps, open the file config / application.config.php. And add the module with the name DTUXMail.


Using the HitMail
---------------------

 - Add and configure the code below into the file that is module.config.php
 - Authentication Options 'transportSsl' and type of connection between ssl and tls

```

    'HitMail' => array(
        'transport' => array(
            'smtpOptions' => array(
                'host' => 'smtp.domain.com',
                'port' => 587,
                'connection_class' => 'plain',
                'connection_config' => array(
                    'username' => 'user@domain.com',
                    'password' => 'password',
                    'from' => 'anyemail@domain.com'
                ),
            ),
            'transportSsl' => array(
                'use_ssl' => false,
                'connection_type' => 'tls' // ssl, tls
            ),
        )
        
    )
    
````

How to use
-----------

 - You need to create the html template within the module that sends the email, exemplo:


```

    application/view/mailtemplates/teste.phtml
    
```    


 - You can insert variables into the view template using the setData code sending email below, for example:


```

    $mail = new Mail($this->getServiceLocator()->get('servicemanager'));
    
    $mail->setData(array('nome' => 'Diego Pereira Grassato', 'email' => 'diego.grassato@gmail.com'))
    
```

- Html the template


```
 
    Teste de envio de email pelo Modulo HitMail.
    
    Nome: <?php echo $this->nome?> <br>
    Email: <?php echo $this->email?>
 
```
 
- Full PHP code to instantiate the class mail and send the mail.


```

    $mail =  $this->getServiceLocator()->get('hit.email');

    $mail->setSubject('Teste de envio de email pelo modulo Mail')
        ->setTo('diego.grassato@gmail.com')
        ->setFrom('diego.grassato@gmail.com')
        ->setData(array('nome' => 'Diego', 'email' => 'diego.grassato@gmail.com'))
        ->setViewTemplate('teste')
        ->send();
        
```

- Use mandrill


```

    $mail =  $this->getServiceLocator()->get('hit.mail.mandrill');

    $mail->setSubject('Teste de envio de email pelo modulo Mail')
        ->setTo('diego.grassato@gmail.com')
        ->setFrom('diego.grassato@gmail.com')
        ->setData(array('nome' => 'Diego', 'email' => 'diego.grassato@gmail.com'))
        ->setViewTemplate('teste')
        ->send();
        
```