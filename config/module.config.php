<?php
return array(
    'HitMail' => array(
        'options' => array(
            'type' => 'text/html',
            'html_encoding' => \Zend\Mime\Mime::ENCODING_8BIT,
            'message_encoding' => 'UTF8'
        )
    ),
    'service_manager' => array(
        'factories' => array(
            'hit.mail' => 'HitMail\Service\Factory\Mail',
            'hit.mail.mandrill' => 'HitMail\Service\Factory\Mandrill',
        )
    )
);